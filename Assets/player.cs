﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    [SerializeField]
    private float jumpPower;
    private bool isJumping;
    private Rigidbody body;

	void Start ()
    {
        isJumping = false;
        body = GetComponent<Rigidbody>();
	}

	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Space) && isJumping == false)
        {
            body.AddForce(Vector3.up * jumpPower);
            isJumping = true;
        }
	}
    void OnCollisionEnter(Collision col)
    {
        isJumping = false;
    }
}
